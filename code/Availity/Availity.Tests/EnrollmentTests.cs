using System;
using System.Collections.Generic;
using System.Linq;
using Availity.Business;
using Availity.Business.Models;
using Availity.Business.Services;
using Moq;
using Xunit;

namespace Availity.Tests
{
    public class EnrollmentTests
    {
        public Mock<ICsvParserService> MockService { get; set; }

        public EnrollmentTests()
        {
            MockService = new Mock<ICsvParserService>();
        }

        [Fact]
        public void it_should_order_by_lastname_firstname()
        {
            var data = new List<EnrollmentModel>()
            {
                new EnrollmentModel()
                {
                    Id = "A1",
                    FullName = "Bob B",
                    Version = 1,
                    InsuranceCompany = "Company A"
                },
                new EnrollmentModel()
                {
                    Id = "A2",
                    FullName = "David A",
                    Version = 1,
                    InsuranceCompany = "Company A"
                }
            };

            RunMockTests(data);

            MockService.Verify(
                v => v.WriteCsvFile(It.IsAny<string>(),
                    It.Is<List<EnrollmentModel>>(x => x.Last().FullName == data.First().FullName)), Times.Once);
        }

        [Fact]
        public void it_should_separate_file_by_insurance_company()
        {
            var data = new List<EnrollmentModel>()
            {
                new EnrollmentModel()
                {
                    Id = "A1",
                    FullName = "Bob A",
                    Version = 1,
                    InsuranceCompany = "Company A"
                },
                new EnrollmentModel()
                {
                    Id = "A1",
                    FullName = "Bob A",
                    Version = 2,
                    InsuranceCompany = "Company B"
                }
            };

            RunMockTests(data);

            MockService.Verify(
                v => v.WriteCsvFile("data/Enrollment_Company A.csv", It.IsAny<List<EnrollmentModel>>()), Times.Once);
            MockService.Verify(
                v => v.WriteCsvFile("data/Enrollment_Company B.csv", It.IsAny<List<EnrollmentModel>>()), Times.Once);
        }

        [Fact]
        public void it_should_remove_duplicate_id_keeping_highest_version()
        {
            var data = new List<EnrollmentModel>()
            {
                new EnrollmentModel()
                {
                    Id = "A1",
                    FullName = "Bob A",
                    Version = 1,
                    InsuranceCompany = "Company A"
                },
                new EnrollmentModel()
                {
                    Id = "A1",
                    FullName = "Bob A",
                    Version = 2,
                    InsuranceCompany = "Company A"
                }
            };

            RunMockTests(data);

            MockService.Verify(
                v => v.WriteCsvFile(It.IsAny<string>(),
                    It.Is<List<EnrollmentModel>>(x => x.Count == 1 && x.TrueForAll(_ => _.Version == 2))), Times.Once);
        }

        private void RunMockTests(List<EnrollmentModel> data)
        {
            MockService.Setup(a => a.ReadCsvFile("test")).Returns(data);
            var processor = new CsvProcessor(MockService.Object);
            processor.GetEnrollments("");
            processor.WriteEnrollments(data);
        }
    }
}
