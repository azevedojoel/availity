﻿using Availity.Business;
using Xunit;

namespace Availity.Tests
{
    public class ParenthesesTests
    {

        [Fact]
        public void it_should_check_parentheses()
        {
            var checker = new ParenthesesChecker();

            Assert.True(checker.IsValid(""));
            Assert.True(checker.IsValid(null));
            Assert.True(checker.IsValid("((((A))))"));
            Assert.False(checker.IsValid("(((A))))"));
        }

        [Fact]
        public void it_should_check_brackets_too()
        {
            var checker = new ParenthesesChecker('{', '}');

            Assert.True(checker.IsValid(""));
            Assert.True(checker.IsValid(null));
            Assert.True(checker.IsValid("{{{{A}}}}"));
            Assert.False(checker.IsValid("{{{()((()()A}}"));
        }

    }
}
