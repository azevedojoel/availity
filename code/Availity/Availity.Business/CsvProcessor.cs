using System.Collections.Generic;
using System.Linq;
using Availity.Business.Models;
using Availity.Business.Services;

namespace Availity.Business
{
    public class CsvProcessor
    {
        private ICsvParserService CsvParserService { get; }

        public CsvProcessor(ICsvParserService csvParserService)
        {
            this.CsvParserService = csvParserService;
        }

        public IEnumerable<EnrollmentModel> GetEnrollments(string path)
        {
            return CsvParserService.ReadCsvFile(path);
        }

        public void WriteEnrollments(List<EnrollmentModel> enrollments, string directory = "data")
        {
            if (enrollments == null || !enrollments.Any())
                return;

            var enrollmentsGroupedByCompany = enrollments.GroupBy(v => v.InsuranceCompany);
            foreach (var group in enrollmentsGroupedByCompany)
            {
                var output = group.ToList()
                    .DistinctEnrollmentsById()
                    .OrderEnrollmentsByName();

                CsvParserService.WriteCsvFile($"{directory}/Enrollment_{group.Key.GetValidFileName()}.csv",
                    output.ToList());
            }
        }
    }
}