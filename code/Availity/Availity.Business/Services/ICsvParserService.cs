using System.Collections.Generic;
using Availity.Business.Models;

namespace Availity.Business.Services
{
    public interface ICsvParserService
    {
        IEnumerable<EnrollmentModel> ReadCsvFile(string file);
        void WriteCsvFile(string path, IEnumerable<EnrollmentModel> enrollmentModels);
    }
}