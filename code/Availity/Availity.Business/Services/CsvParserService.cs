using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Availity.Business.Mappers;
using Availity.Business.Models;
using CsvHelper;

namespace Availity.Business.Services
{
    public class CsvParserService : ICsvParserService
    {
        public IEnumerable<EnrollmentModel> ReadCsvFile(string file)
        {
            try
            {
                using (var reader = new StreamReader(file, Encoding.Default))
                {
                    using (var csv = new CsvReader(reader, CultureInfo.CurrentCulture))
                    {
                        csv.Configuration.RegisterClassMap<EnrollmentMap>();
                        return csv.GetRecords<EnrollmentModel>().ToList();
                    }
                }
            }
            catch (Exception)
            {
                return new List<EnrollmentModel>();
            }
        }

        public void WriteCsvFile(string path, IEnumerable<EnrollmentModel> enrollmentModels)
        {
            using (var sw = new StreamWriter(path, false, new UTF8Encoding(true)))
            {
                using (var cw = new CsvWriter(sw, CultureInfo.CurrentCulture))
                {
                    cw.WriteHeader<EnrollmentModel>();
                    cw.NextRecord();
                    foreach (var enrollment in enrollmentModels)
                    {
                        cw.WriteRecord<EnrollmentModel>(enrollment);
                        cw.NextRecord();
                    }
                }
            }
        }
    }
}