using System.Collections.Generic;
using System.Linq;
using Availity.Business.Models;

namespace Availity.Business
{
    public static class Extensions
    {
        public static string GetValidFileName(this string fileName)
        {
            var invalidChars = System.IO.Path.GetInvalidFileNameChars();
            var cleanFileName = new string(fileName.Where(m => !invalidChars.Contains(m)).ToArray<char>());
            return cleanFileName;
        }

        public static IEnumerable<EnrollmentModel> DistinctEnrollmentsById(this IEnumerable<EnrollmentModel> enrollments)
        {
            return enrollments.GroupBy(g => g.Id)
                .SelectMany(g => g.Where(i => i.Version == g.Max(m => m.Version)))
                .Distinct();
        }

        public static IEnumerable<EnrollmentModel> OrderEnrollmentsByName(this IEnumerable<EnrollmentModel> enrollments)
        {
            return enrollments.OrderBy(v => v.FullName.Split(' ')[1])
                .ThenBy(v => v.FullName.Split(' ')[0]);
        }

    }
}
