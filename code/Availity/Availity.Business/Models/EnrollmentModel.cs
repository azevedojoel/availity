
using CsvHelper.Configuration.Attributes;

namespace Availity.Business.Models
{
  public class EnrollmentModel
  {
    [Name(nameof(Id))]
    public string Id { get; set; }

    [Name(nameof(FullName))]
    public string FullName { get; set; }

    [Name(nameof(Version))]
    public int Version { get; set; }

    [Name(nameof(InsuranceCompany))]
    public string InsuranceCompany { get; set; }
  }
}