using System.Collections.Generic;

namespace Availity.Business
{
    public class ParenthesesChecker
    {
        public ParenthesesChecker(char opener = '(', char closer = ')')
        {
            LeftSide = opener;
            RightSide = closer;
        }

        public static char LeftSide { get; set; } = '(';
        public static char RightSide { get; set; } = ')';

        public bool IsValid(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return true;
            }

            var counter = 0;
            foreach (var item in input)
            {
                if (item == LeftSide)
                {
                    counter++;
                }
                else if (item == RightSide)
                {
                    if (counter == 0)
                    {
                        return false;
                    }

                    counter--;
                }
            }

            return counter <= 0;
        }
    }
}