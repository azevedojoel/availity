using Availity.Business.Models;
using CsvHelper.Configuration;

namespace Availity.Business.Mappers
{
  public sealed class EnrollmentMap : ClassMap<EnrollmentModel>
  {
    public EnrollmentMap()
    {
      Map(m => m.Id).Name(nameof(EnrollmentModel.Id));
      Map(m => m.FullName).Name(nameof(EnrollmentModel.FullName));
      Map(m => m.Version).Name(nameof(EnrollmentModel.Version));
      Map(m => m.InsuranceCompany).Name(nameof(EnrollmentModel.InsuranceCompany));
    }
  }
}