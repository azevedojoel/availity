﻿using System;
using System.Linq;
using Availity.Business;
using Availity.Business.Services;
using SimpleInjector;

namespace Availity.App
{
    internal class Program
    {
        
        private static readonly Container Container;

        static Program()
        {
            Container = new Container();
            Container.Register<ICsvParserService, CsvParserService>();
            Container.Verify();
        }

        private static void Main(string[] args)
        {
            Console.WriteLine("Processing CSV File");
            var processor = new CsvProcessor(Container.GetInstance<ICsvParserService>());
            var enrollments = processor.GetEnrollments("Data/Enrollments_in.csv");
            processor.WriteEnrollments(enrollments.ToList());

            Console.WriteLine("Checking For Balanced Parentheses");
            var checker = new ParenthesesChecker();
            var inputTrue = "(((A)))";
            var inputFalse = "(((A))";
            Console.WriteLine($"Is {inputTrue} valid: {checker.IsValid(inputTrue)}");
            Console.WriteLine($"Is {inputFalse} valid: {checker.IsValid(inputFalse)}");
        }
    }
}
