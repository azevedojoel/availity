export class Provider {
  constructor(
    public name: string,
    public npi: string,
    public address: string,
    public phone: string,
    public email: string
  ) { }
}
