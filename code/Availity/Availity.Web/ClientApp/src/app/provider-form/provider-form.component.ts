import { Component, OnInit } from '@angular/core';
import { Provider } from '../provider';

@Component({
  selector: 'app-provider-form',
  templateUrl: './provider-form.component.html',
  styleUrls: ['./provider-form.component.less']
})
export class ProviderFormComponent implements OnInit {

  model = new Provider('', '', '', '', '');

  onSubmit() { }

  get diagnostic() { return JSON.stringify(this.model); }

  constructor() { }

  ngOnInit(): void {
  }

}
