-- 7.a)

Select
	CustID,
	FirstName,
	LastName
From 
	Customer
Where
	LastName like 'S%'
Order By
	LastName Desc, FirstName Desc


-- 7.b)

Select
	CustID,
	FirstName,
	LastName,
	coalesce(sum(Cost * Quantity), 0) as Total
From Customer c
	left join [Order] o on c.CustID = o.CustomerID
	left join OrderLine ol on o.OrderID = ol.OrdID
Where
	OrderDate is null OR OrderDate >= dateadd(month, -6, getdate())
Group By
	CustID,
	FirstName,
	LastName,
	OrdID
Order By
	LastName,
	FirstName

-- 7.c)

Select
	CustID,
	FirstName,
	LastName,
	sum(Cost * Quantity) as Total
From Customer c
	inner join [Order] o on c.CustID = o.CustomerID
	inner join OrderLine ol on o.OrderID = ol.OrdID
Where
	OrderDate is null OR OrderDate >= dateadd(month, -6, getdate())
Group By
	CustID,
	FirstName,
	LastName,
	OrdID
Having
	sum(Cost * Quantity) between 100 and 500
Order By
	LastName,
	FirstName